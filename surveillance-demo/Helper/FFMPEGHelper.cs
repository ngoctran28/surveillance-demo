﻿using System;
using System.Diagnostics;
using System.IO;

namespace surveillance_demo.Helper
{
    public static class FFMPEGHelper
    {
        public static Process StartProcess(params string[] arguments)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    CreateNoWindow = false,
                    UseShellExecute = false,
                    FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Libs\\ffmpeg\\ffmpeg.exe"),
                    Arguments = string.Join(' ', arguments),
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                },
                EnableRaisingEvents = true
            };

            process.OutputDataReceived += (sender, e) =>
            {
                // Prepend line numbers to each line of the output.
                if (!string.IsNullOrEmpty(e.Data))
                {
                    Console.WriteLine(e.Data);
                }
            };

            process.ErrorDataReceived += (sender, e) =>
            {
                // Prepend line numbers to each line of the output.
                if (!string.IsNullOrEmpty(e.Data))
                {
                    Console.WriteLine($"{e.Data}");
                }
            };

            Console.WriteLine($"Executing \"{process.StartInfo.FileName}\" with arguments \"{process.StartInfo.Arguments}\".\r\n");

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            return process;
        }
    }
}
