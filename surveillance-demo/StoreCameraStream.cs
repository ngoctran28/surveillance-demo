using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using surveillance_demo.Helper;
using DateTime = System.DateTime;

namespace surveillance_demo
{
    public class StoreCameraStream
    {
        private static readonly Dictionary<string, Process> Processes = new Dictionary<string, Process>();

        //save stream to file
        public static readonly Dictionary<string, string> CameraStreams = new Dictionary<string, string>
        {
            {"cam1", "http://202.142.12.41/nphMotionJpeg?Resolution=640x480&Quality=Clarity"},
            {"cam2", "http://cam6284208.miemasu.net/nphMotionJpeg?Resolution=640x480&Quality=Clarity"},
            {"cam3", "http://192.82.150.11:8086/mjpg/video.mjpg"}
        };

        private static string OutputDir => Path.Combine(Directory.GetCurrentDirectory(), "output-stream");

        public static void SaveStreamFile()
        {
            //start record
            foreach (var stream in CameraStreams)
            {
                RecordStream(stream);
            }
        }

        private static void RecordStream(KeyValuePair<string, string> stream)
        {
            //split file for each 1 minute
            var splitArgs = "-c copy -map 0 -segment_time 00:01:00 -f segment";

            var output = $"{OutputDir}\\{DateTime.Now:yyyy-MM-dd}\\{stream.Key}";
            if (!Directory.Exists(output)) Directory.CreateDirectory(output);

            var process = FFMPEGHelper.StartProcess("-i", stream.Value, splitArgs, $"-y {output}\\{DateTime.Now:HHmm}-%03d.mp4");
            //stop old process
            Processes.Add(stream.Key, process);
        }
    }
}
